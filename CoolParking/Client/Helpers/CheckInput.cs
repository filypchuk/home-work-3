﻿using CoolParking.BL.Models;
using System;
using System.Text.RegularExpressions;

namespace Client.Helpers
{
    class CheckInput
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        public CheckInput()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
        }
        public int CheckingInt()
        {
            bool flag;
            int number;
            do
            {
                string value = Console.ReadLine();
                flag = Int32.TryParse(value, out number);  // If the entered data is incorrect, you must enter it again
                if (!flag)
                {
                    print("Value isn't correct (it must be integer value)", Color.Red);
                }
                else if (number <= 0)                      // Additional check for a positive number
                {
                    flag = false;
                    print("Value must be positive and bigger than 0 ", Color.Red);
                }
            }
            while (!flag);
            return number;
        }
        public double CheckingDouble()
        {
            bool flag;
            double number;
            do
            {
                string value = Console.ReadLine();
                flag = double.TryParse(value, out number);
                if (!flag)
                {
                    print("Value isn't correct ", Color.Red);
                    print("You can only enter numbers and coma \",\"", Color.Red);
                }
                else if (number <= 0)
                {
                    flag = false;
                    print("Value must be positive and bigger than 0 ", Color.Red);
                }
            }
            while (!flag);
            return number;
        }
        public decimal CheckingDecimal()
        {
            bool flag;
            decimal number;
            do
            {
                string value = Console.ReadLine();
                flag = decimal.TryParse(value, out number);
                if (!flag)
                {
                    print("Value isn't correct ", Color.Red);
                    print("You can only enter numbers and coma \",\"", Color.Red);
                }
                else if (number <= 0)
                {
                    flag = false;
                    print("Value must be positive and bigger than 0 ", Color.Red);
                }
            }
            while (!flag);
            return number;
        }
        public VehicleType ChekingVehicleType()
        {
            int vehicleType = CheckingInt();
            while (vehicleType < 1 || vehicleType > 4)
            {
                print("The number should be from 1 to 4 and integer", Color.Red);
                vehicleType = CheckingInt();
            }
            switch (vehicleType)
            {
                case 1: return VehicleType.PassengerCar;
                case 2: return VehicleType.Truck;
                case 3: return VehicleType.Bus;
                case 4: return VehicleType.Motorcycle;
                default:
                    break;
            }
            return VehicleType.Bus;
        }
        public string CheckRegistrationPlateNumber()
        {
            string plateNumber ="";
            string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

            while (!Regex.IsMatch(plateNumber, pattern))
            {
                plateNumber = Console.ReadLine();
                if (!Regex.IsMatch(plateNumber, pattern))
                {
                    print("Format must be XX-YYYY-XX where X-capital letter, Y-number from 0 to 9", Color.Red);
                }
            }

            return plateNumber;
        }
    }
}
