﻿using Client.Helpers;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Client.HttpRequests
{
    public class ParkingRequests
    {
        private readonly HttpClient _client;
        public ParkingRequests()
        {
            _client = new HttpClient();
        }
        public decimal GetBalance()
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/parking/balance");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/parking/balance |--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");

            string str = resault.Result.Content.ReadAsStringAsync().Result;
            decimal velue = JsonConvert.DeserializeObject<decimal>(str);
            return velue;
        }
        public int GetCapacity()
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/parking/capacity");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/parking/capacity | --------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");

            string str = resault.Result.Content.ReadAsStringAsync().Result;
            int velue = JsonConvert.DeserializeObject<int>(str);
            return velue;
        }
        public int GetFreePlaces()
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/parking/freePlaces");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/parking/freePlaces | --------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");

            string str = resault.Result.Content.ReadAsStringAsync().Result;
            int velue = JsonConvert.DeserializeObject<int>(str);
            return velue;
        }
    }
}
