﻿using Client.Helpers;
using CoolParking.BL.Dto;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Client.HttpRequests
{
    public class TransactionsRequests
    {
        private readonly HttpClient _client;
        public TransactionsRequests()
        {
            _client = new HttpClient();
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/transactions/last");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/transactions/last |--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");
            string str = resault.Result.Content.ReadAsStringAsync().Result;
            TransactionInfo[] transactions = JsonConvert.DeserializeObject<TransactionInfo[]>(str);
            return transactions;
        }
        public string AllFromLog()
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/transactions/all");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/transactions/all |--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");
            return resault.Result.Content.ReadAsStringAsync().Result;
        }
        public void TopUpVehicle(string id, decimal sum)
        {
            var json = JsonConvert.SerializeObject(new TopUpVehicleDto { Id = id, Sum = sum });
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var resault = _client.PutAsync($"{Url.BaseUrl}/transactions/topUpVehicle", stringContent);
            Console.WriteLine($"--------|PUT: {Url.BaseUrl}/transactions/topUpVehicle |--------");
            Console.WriteLine($"--------|JSON: {json}|--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");

        }
    }
}
