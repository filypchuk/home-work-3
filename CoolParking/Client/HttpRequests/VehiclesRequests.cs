﻿using Client.Helpers;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;

namespace Client.HttpRequests
{
    public class VehiclesRequests
    {
        private readonly HttpClient _client;
        public VehiclesRequests()
        {
            _client = new HttpClient();
        }
        public ReadOnlyCollection<Vehicle> GetAll()
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/vehicles");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/vehicles |--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");

            string str = resault.Result.Content.ReadAsStringAsync().Result;
            ReadOnlyCollection<Vehicle> vehicles = JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(str);
            return vehicles;
        }
        public Vehicle GetById(string id)
        {
            var resault = _client.GetAsync($"{Url.BaseUrl}/vehicles/{id}");
            Console.WriteLine($"--------|GET: {Url.BaseUrl}/vehicles/{id} |--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");

            string str = resault.Result.Content.ReadAsStringAsync().Result;
            Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(str);
            return vehicle;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            var json = JsonConvert.SerializeObject(vehicle);
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var resault = _client.PostAsync($"{Url.BaseUrl}/vehicles", stringContent);
            Console.WriteLine($"--------|POST: {Url.BaseUrl}/vehicles |--------");
            Console.WriteLine($"--------|JSON: {json}|--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");
        }
        public void DeleteVehicle(string id)
        {
            var resault = _client.DeleteAsync($"{Url.BaseUrl}/vehicles/{id}");
            Console.WriteLine($"--------|DELETE: {Url.BaseUrl}/vehicles/{id} |--------");
            Console.WriteLine(resault.Result);
            Console.WriteLine("---------------------------------------------------------------------");
        }
    }
}
