﻿using Client.Helpers;
using Client.HttpRequests;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Client.Menu
{
    class ParkingMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;

        private readonly ParkingRequests _parkingRequests;
        private readonly VehiclesRequests _vehiclesRequests;
        private readonly TransactionsRequests _transactionsRequests;
        public ParkingMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();

            _parkingRequests = new ParkingRequests();
            _vehiclesRequests = new VehiclesRequests();
            _transactionsRequests = new TransactionsRequests();

        }

        public void ParkingBalance()
        {
            Console.Clear();
            print($"Parking balance - {_parkingRequests.GetBalance()}$", Color.Green);
            printWaitingForEnter();
        }
        public void SumLastParkingTransactions()
        {
            Console.Clear();
            print($"The amount of money earned for the current period - " +
                $"{_transactionsRequests.GetLastParkingTransactions().Sum(tr => tr.Sum)}$", Color.Green);
            printWaitingForEnter();
        }
        public void InfoParkingSpaces()
        {
            Console.Clear();
            print($"Free parking spaces - {_parkingRequests.GetFreePlaces()}/{_parkingRequests.GetCapacity()} ", Color.Green);
            printWaitingForEnter();
        }
        public void LastParkingTransactions()
        {
            Console.Clear();
            TransactionInfo[] lastParkingTransactions = _transactionsRequests.GetLastParkingTransactions();

            if (lastParkingTransactions.Length != 0)
            {
                print("All Parking Transactions for the current period", Color.Green);
                for (int i = 0; i < lastParkingTransactions.Length; i++)
                {
                    print($"{i + 1} - { lastParkingTransactions[i].TransactionDate.ToString("MM/dd/yyyy h:mm tt")}: " +
                    $"{lastParkingTransactions[i].Sum} money withdrawn from vehicle with " +
                    $"Id='{lastParkingTransactions[i].VehicleId}'.", Color.Yellow);
                }
            }
            else
            {
                print("Now list is empty", Color.Red);
            }
            printWaitingForEnter();
        }
        public void ReadTransactionsLog()
        {
            Console.Clear();
            print("Transaction history of Transactions.log ", Color.Green);
            string text = _transactionsRequests.AllFromLog();
            if (text != "")
            {
                print($"{text}", Color.Yellow);
            }
            else
            {
                print("Transactions.log is empty", Color.Red);
            }
            printWaitingForEnter();
        }
        public void AllVehicles()
        {
            Console.Clear();
            ShowAllVehiclesOnParking();
            printWaitingForEnter();
        }
        public void AddVehicle()
        {
            Console.Clear();
            print("Put the vehicle in the parking lot", Color.Green);
            if (_parkingRequests.GetFreePlaces() > 0)
            {
                print("Selecte a vehicle type click button 1-4", Color.Green);
                print("1-Passenger Car  2-Truck  3-Bus  4-Motorcycle", Color.Green);
                VehicleType vehicleType = checkInput.ChekingVehicleType();
                print("Enter balance - ", Color.Green);
                decimal balance = checkInput.CheckingDecimal();
                string vehicleId = Vehicle.GenerateRandomRegistrationPlateNumber();
                Vehicle newVehicle = new Vehicle(vehicleId, vehicleType, balance);
                _vehiclesRequests.AddVehicle(newVehicle);
            }
            else
            {
                print("In the parking not free spaces, you can't to put vehicle, ", Color.Red);
            }
            printWaitingForEnter();
        }
        public void RemoveVehicle()
        {
            Console.Clear();
            ReadOnlyCollection<Vehicle> readOnlyVehicles = _vehiclesRequests.GetAll();
            if (readOnlyVehicles.Count == 0)
            {
                print("There is no vehicle in the parking lot", Color.Red);
            }
            else
            {
                ShowAllVehiclesOnParking();
                print("Removing the Vehicle from the Parking", Color.Green);
                print("Enter Registration Plate Number", Color.Green);
                string vehicteId = checkInput.CheckRegistrationPlateNumber();
                Vehicle vehicle = readOnlyVehicles.FirstOrDefault(v => v.Id == vehicteId);
                if (vehicle != null)
                {
                    if (vehicle.Balance >= 0)
                    {
                        _vehiclesRequests.DeleteVehicle(vehicteId);
                    }
                    else
                    {
                        print($"Vehicle with Registration Plate Number - {vehicteId} balance - {vehicle.Balance}$", Color.Red);
                        print($"For removing, top up the balance - a vehicle with a debt balance cannot be removed", Color.Red);
                    }
                }
                else
                {
                    print($"Vehicle with Registration Plate Number - \"{vehicteId}\" not found", Color.Red);
                }
            }
            printWaitingForEnter();
        }
        public void TopUpBalance()
        {
            Console.Clear();
            ReadOnlyCollection<Vehicle> readOnlyVehicles = _vehiclesRequests.GetAll();
            if (readOnlyVehicles.Count == 0)
            {
                print("There is no vehicle in the parking lot", Color.Red);
            }
            else
            {
                ShowAllVehiclesOnParking();
                print("Replenishment of the vehicle balance", Color.Green);
                print("Enter Registration Plate Number", Color.Green);
                string vehicteId = checkInput.CheckRegistrationPlateNumber();
                Vehicle vehicle = readOnlyVehicles.FirstOrDefault(v => v.Id == vehicteId);
                if (vehicle != null)
                {
                    print("Enter the amount, which you want to add", Color.Green);
                    decimal balance = checkInput.CheckingDecimal();
                    _transactionsRequests.TopUpVehicle(vehicteId, balance);
                }
                else
                {
                    print($"Vehicle with Registration Plate Number - \"{vehicteId}\" not found", Color.Red);
                }
            }
            printWaitingForEnter();
        }
        private void ShowAllVehiclesOnParking()
        {
            ReadOnlyCollection<Vehicle> readOnlyVehicles = _vehiclesRequests.GetAll();
            if (readOnlyVehicles.Count == 0)
            {
                print("There is no vehicle in the parking lot", Color.Red);
            }
            else
            {
                print("List of all vehicles in the parking lot", Color.Green);
                int i = 1;
                foreach (Vehicle vehicle in readOnlyVehicles)
                {
                    print($"{i}. Type - {vehicle.VehicleType}, Vehicle id - {vehicle.Id}, " +
                        $"Balance - {vehicle.Balance}$", Color.Yellow);
                    i++;
                }
            }
        }
    }
}
