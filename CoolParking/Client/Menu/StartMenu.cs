﻿using Client.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Menu
{
    class StartMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly ParkingMenu _parkingMenu;
        public StartMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            _parkingMenu = new ParkingMenu();
        }
        public void ParkingMenu()
        {
            DisplayParkingMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        _parkingMenu.ParkingBalance();
                        break;
                    case ConsoleKey.D2:
                        _parkingMenu.SumLastParkingTransactions();
                        break;
                    case ConsoleKey.D3:
                        _parkingMenu.InfoParkingSpaces();
                        break;
                    case ConsoleKey.D4:
                        _parkingMenu.LastParkingTransactions();
                        break;
                    case ConsoleKey.D5:
                        _parkingMenu.ReadTransactionsLog();
                        break;
                    case ConsoleKey.D6:
                        _parkingMenu.AllVehicles();
                        break;
                    case ConsoleKey.D7:
                        _parkingMenu.AddVehicle();
                        break;
                    case ConsoleKey.D8:
                        _parkingMenu.RemoveVehicle();
                        break;
                    case ConsoleKey.D9:
                        _parkingMenu.TopUpBalance();
                        break;
                    default:
                        DisplayParkingMenu();
                        break;
                }
                DisplayParkingMenu();
            }
        }
        private void DisplayParkingMenu()
        {
            Console.Clear();
            print("Click button 1-9", Color.Red);
            print(" ", Color.None);
            print("1---Display parking balance.", Color.Green);
            print(" ", Color.None);
            print("2---Display the amount of earned funds for the current period.", Color.Green);
            print(" ", Color.None);
            print("3---Display the number of free parking spaces.", Color.Green);
            print(" ", Color.None);
            print("4---Display all Parking Transactions for the current period.", Color.Green);
            print(" ", Color.None);
            print("5---Display the transaction history of Transactions.log .", Color.Green);
            print(" ", Color.None);
            print("6---Display a list vehicles which are in the Parking lot.", Color.Green);
            print(" ", Color.None);
            print("7---Put the vehicle in the parking lot.", Color.Green);
            print(" ", Color.None);
            print("8---Remove the Vehicle from the Parking.", Color.Green);
            print(" ", Color.None);
            print("9---Top Up balance for a Vehicle.", Color.Green);
            print(" ", Color.None);
        }
    }
}
