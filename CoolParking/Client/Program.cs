﻿using Client.HttpRequests;
using Client.Menu;
using CoolParking.BL.Models;
using System;
using System.Net.Http;

namespace Client
{
    class Program
    {
        
        static void Main(string[] args)
        {
            StartMenu start = new StartMenu();
            start.ParkingMenu();
        }
    }
}
