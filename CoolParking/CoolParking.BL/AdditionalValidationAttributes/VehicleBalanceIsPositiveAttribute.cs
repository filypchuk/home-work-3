﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.BL.AdditionalValidationAttributes
{
    public class VehicleBalanceIsPositiveAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            Nullable<decimal> balance = value as decimal?;

            if (balance == null || balance <= 0)
            {
                this.ErrorMessage = "Balance must be positive";
                return false;
            }
            return true;
        }
    }
}
