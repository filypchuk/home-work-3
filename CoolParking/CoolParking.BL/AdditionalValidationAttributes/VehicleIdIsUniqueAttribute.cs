﻿using CoolParking.BL.Models;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.BL.AdditionalValidationAttributes
{
    public class VehicleIdIsUniqueAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string id = value as string;

            if (Parking.Instance.ListVehicles.Exists(v => v.Id == id))
            {
                this.ErrorMessage = "Vehicle Id must be Unique";
                return false;
            }
            return true;
        }
    }
}
