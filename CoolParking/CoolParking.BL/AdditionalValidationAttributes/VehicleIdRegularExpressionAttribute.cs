﻿using CoolParking.BL.Models;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CoolParking.BL.AdditionalValidationAttributes
{
    public class VehicleIdRegularExpressionAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string str = value as string;
            string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
            if (str == null || !Regex.IsMatch(str, pattern))
            {
                this.ErrorMessage = "Format must be XX-YYYY-XX where X-capital letter, Y - number from 0 to 9";
                return false;
            }
            return true;
        }
    }
}
