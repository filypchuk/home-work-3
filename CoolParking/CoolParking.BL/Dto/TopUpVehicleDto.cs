﻿using CoolParking.BL.AdditionalValidationAttributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoolParking.BL.Dto
{
    public class TopUpVehicleDto
    {
        [JsonProperty("id")]
        [VehicleIdRegularExpression]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        [VehicleBalanceIsPositive]
        public decimal Sum { get; set; }

    }
}
