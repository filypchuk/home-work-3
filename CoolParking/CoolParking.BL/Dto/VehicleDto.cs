﻿using CoolParking.BL.AdditionalValidationAttributes;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.BL.Dto
{
    public class VehicleDto
    {
        [JsonProperty("id")]
        [VehicleIdRegularExpression]
        [VehicleIdIsUnique]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        [Required(ErrorMessage = "VehicleType is required.")]
        [Range(1, 4)]
        public VehicleType VehicleType { get; set; }
        [JsonProperty("balance")]
        [VehicleBalanceIsPositive]
        public decimal Balance { get; set; }
    }
}
