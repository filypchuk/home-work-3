﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal parkingBalance = 0m;
        public static int parkingCapacity = 10;
        public static double intervalForPay = 5;
        public static decimal fine = 2.5m;
        public static double intervalWriteIntoLog = 60; 
        public static Dictionary<VehicleType, decimal> vehiclePrice = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2 },  
            { VehicleType.Truck, 5 }, 
            { VehicleType.Bus, 3.5m }, 
            { VehicleType.Motorcycle, 1 },
        };
    }
}