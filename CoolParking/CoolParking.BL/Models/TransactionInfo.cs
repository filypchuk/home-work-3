﻿using Newtonsoft.Json;
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; private set; }
        [JsonProperty("sum")]
        public decimal Sum { get; private set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; private set; }

        public TransactionInfo(DateTime transactionDate, string vehicleId, decimal cost)
        {
            TransactionDate = transactionDate;
            VehicleId = vehicleId;
            Sum = cost;
        }

    }
}