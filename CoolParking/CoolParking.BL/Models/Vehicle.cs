﻿using CoolParking.BL.AdditionalValidationAttributes;
using System.Text.Json;
using System.Text.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get;  }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
            if (Regex.IsMatch(id, pattern) && balance >= 0)            
            {
                Id = id;
                VehicleType = vehicleType;
                Balance = balance;
            }
            else
            {
                throw new ArgumentException("Vehicle is not valid");
            }

        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            List<Vehicle> vehicles = Parking.Instance.ListVehicles;
            string numeric = "0123456789";
            string letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            char[] marks = new char[10];
            string registrationPlateNumber = "";
            do
            {
                marks[0] = GetLetter(letter);
                marks[1] = GetLetter(letter);
                marks[2] = '-';
                marks[3] = GetLetter(numeric);
                marks[4] = GetLetter(numeric);
                marks[5] = GetLetter(numeric);
                marks[6] = GetLetter(numeric);
                marks[7] = '-';
                marks[8] = GetLetter(letter);
                marks[9] = GetLetter(letter);

                registrationPlateNumber = new string(marks);
            }
            while (vehicles.Exists(vehicle => vehicle.Id == registrationPlateNumber));

            return registrationPlateNumber;
        }
        private static char GetLetter(string chars)
        {
            Random rand = new Random();
            int num = rand.Next(chars.Length);
            return chars[num];
        }
    }
}