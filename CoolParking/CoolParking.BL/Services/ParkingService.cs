﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using System.Threading;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly List<TransactionInfo> transactionsHistory; 

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            transactionsHistory = new List<TransactionInfo>();
           _logTimer.Elapsed += (sender, args) => { WriteToLogPeriod(); };
            _withdrawTimer.Interval += 50;
           _withdrawTimer.Elapsed += (sender, args) => { PaymentPeriod(); };

        }

        public decimal GetBalance()
        {
            return Parking.Instance.Balance;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.parkingCapacity - Parking.Instance.ListVehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> readOnlyVehicles = new ReadOnlyCollection<Vehicle>(Parking.Instance.ListVehicles);
            return readOnlyVehicles;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.Instance.ListVehicles.Count < Settings.parkingCapacity)
            {
                if (Parking.Instance.ListVehicles.Exists(v => v.Id == vehicle.Id))
                {
                    throw new ArgumentException("There is vehicle with the same id");
                }
                else
                {
                    Parking.Instance.ListVehicles.Add(vehicle);
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = Parking.Instance.ListVehicles.Find(v => v.Id == vehicleId);
            if (vehicle!=null)
            {
                if (vehicle.Balance >= 0)
                {
                    Parking.Instance.ListVehicles.Remove(vehicle);
                }
                else throw new InvalidOperationException(" You can't remove vehicle with a negative balance");
            }
            else
            {
               throw new ArgumentException("Vehicle for removing not found");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = Parking.Instance.ListVehicles.Find(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException($"Vehicle with id {vehicleId} not found");
            }
            else if (sum < 0)
            {
                throw new ArgumentException("Sum must be positive");
            }
            else
            {
                vehicle.Balance += sum;
            }        
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionsHistory.ToArray();
        }

        public string ReadFromLog()
        {
           return _logService.Read();
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            Parking.Instance.Clear();
        }

        public void PaymentPeriod()
        {
            foreach (Vehicle vehicle in Parking.Instance.ListVehicles)
            {
                decimal cost = CalculateCost(vehicle);
                vehicle.Balance -= cost;
                Parking.Instance.Balance += cost;
                transactionsHistory.Add(new TransactionInfo(DateTime.Now, vehicle.Id, cost));
            }
        }
        private decimal CalculateCost(Vehicle vehicle)
        {
            decimal cost;
            decimal price = Settings.vehiclePrice[vehicle.VehicleType];
            if (vehicle.Balance <= 0)
            {
                cost = price * Settings.fine;
            }
            else if ((vehicle.Balance - price) < 0)
            {
                var notEnough = price - vehicle.Balance;
                cost = vehicle.Balance + notEnough * Settings.fine;                
            }
            else
            {
                cost = price;
            }
            return cost;
        }
        public void WriteToLogPeriod()
        {
            string logInfo = FormatingToWrite();
            _logService.Write(logInfo);
            transactionsHistory.Clear();
        }
        private string FormatingToWrite()
        {
            StringBuilder sb = new StringBuilder();
            var transactions = GetLastParkingTransactions();
            foreach (var trInfo in transactions)
            {
                sb.Append(
                    $"{trInfo.TransactionDate.ToString("MM/dd/yyyy hh:mm:ss tt")}: " +
                    $"{trInfo.Sum} money withdrawn from vehicle with " +
                    $"Id='{trInfo.VehicleId}'.\n");
            }

            sb.Append($"Sum for period transaction - {transactions.Sum(tr => tr.Sum)}\n");
            return sb.ToString();
        }
    }
}