﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{  
    public class TimerService : ITimerService
    {
        private readonly Timer aTimer;
        public double Interval 
        { 
            get 
            { 
                return aTimer.Interval; 
            } 
            set 
            { 
                aTimer.Interval = value; 
            } 
        }
        public event ElapsedEventHandler Elapsed 
        { 
            add 
            {
                aTimer.Elapsed += value; 
            } 
            remove 
            { 
                aTimer.Elapsed -= value; 
            } 
        }

        public TimerService(double interval)
        {
            aTimer = new Timer(interval)
            {
                AutoReset = true,
                Enabled = true
            };
        }

        public void Start()
        {
            aTimer.Start();
        }

        public void Stop()
        {
            aTimer.Stop();
        }
        public void Dispose()
        {
            aTimer.Dispose();
        }

    }
}