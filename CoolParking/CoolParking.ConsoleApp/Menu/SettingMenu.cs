﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.ConsoleApp.Helpers;
using System;

namespace CoolParking.ConsoleApp.Menu
{
    class SettingMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly  Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;
        public SettingMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();
        }
        public void ChengeBalance()
        {
            Console.Clear();
            print($"Starting balance for parking - {Settings.parkingBalance}$", Color.Green);
            print("Enter a new value for the initial balance", Color.Green);
            Settings.parkingBalance = checkInput.CheckingDecimal();
            print($"New balance - {Settings.parkingBalance }$", Color.Yellow);
            printWaitingForEnter();
        }
        public void ChengeMaxSize()
        {
            Console.Clear();
            print($"Now capacity of parking - {Settings.parkingCapacity}", Color.Green);
            print("Enter a new capacity of parking", Color.Green);
            Settings.parkingCapacity = checkInput.CheckingInt();
            print($"New capacity - {Settings.parkingCapacity }$", Color.Yellow);
            printWaitingForEnter();
        }
        public void ChengePaymentInterval()
        {
            Console.Clear();
            print($"Now payment write-off period - {Settings.intervalForPay} seconds", Color.Green);
            print("Enter a new payment write-off period", Color.Green);
            print("It is necessary to set in seconds NOT in milliseconds", Color.Yellow);
            Settings.intervalForPay = checkInput.CheckingDouble();
            print($"New payment write-off period - {Settings.intervalForPay } seconds", Color.Yellow);
            printWaitingForEnter();
        }
        public void ChengeWriteToLogInterval()
        {
            Console.Clear();
            print($"Now period for writing to the Transaction.log - {Settings.intervalWriteIntoLog} seconds", Color.Green);
            print("Enter a new period for writing to the Transaction.log", Color.Green);
            print("It is necessary to set in seconds NOT in milliseconds", Color.Yellow);          
            Settings.intervalWriteIntoLog = checkInput.CheckingDouble();          
            print($"New period for writing to the Transaction.log - {Settings.intervalWriteIntoLog } seconds", Color.Yellow);
            printWaitingForEnter();
        }
        public void ChengeFine()
        {
            Console.Clear();
            print($"Now coefficient of fine {Settings.fine}$", Color.Green);
            print("Enter a new coefficient of fine", Color.Green);
            Settings.fine = checkInput.CheckingDecimal();
            print($"New coefficient of fine - {Settings.fine }$", Color.Yellow);
            printWaitingForEnter();
        }
        public void ChengeCostVehicle()
        {
            Console.Clear();
            print("Now cost for a vehicle", Color.Green);
            print($"1 - Passenger Car - {Settings.vehiclePrice[VehicleType.PassengerCar]}$", Color.Yellow);
            print($"2 - Truck - {Settings.vehiclePrice[VehicleType.Truck]}$", Color.Yellow);
            print($"3 - Bus - {Settings.vehiclePrice[VehicleType.Bus]}$", Color.Yellow);
            print($"4 - Motorcycle - {Settings.vehiclePrice[VehicleType.Motorcycle]}$", Color.Yellow);
            VehicleType vehicle_type = checkInput.ChekingVehicleType();
            print($"Enter new cost for a {vehicle_type}", Color.Green);
            decimal price = checkInput.CheckingDecimal();
            Settings.vehiclePrice[vehicle_type] = price;
            print($"New cost for a {vehicle_type} - {price}$", Color.Green);
            printWaitingForEnter();
        }
    }
}
