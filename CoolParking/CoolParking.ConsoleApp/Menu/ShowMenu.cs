﻿using CoolParking.ConsoleApp.Helpers;
using System;
using CoolParking.BL.Models;
namespace CoolParking.ConsoleApp.Menu
{
    public class ShowMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        public ShowMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
        }

        public void DisplayMainMenu()
        {
            Console.Clear();
            Console.SetCursorPosition(15, 1);
            print("MENU", Color.Green);
            Console.SetCursorPosition(15, 3);
            print("1---Parking", Color.Green);
            Console.SetCursorPosition(15, 5);
            print("2---Settings", Color.Yellow);
            Console.SetCursorPosition(15, 9);
            print("Click button 1-2 for chenge", Color.Red);
        }
        public void DisplaySettingsMenu()
        {
            Console.Clear();
            print("Click button 1-6 for chenge", Color.Yellow);
            print(" ", Color.None);
            print($"1---Change start balance for parking  - {Settings.parkingBalance}$", Color.Yellow);
            print(" ", Color.None);
            print($"2---Change max size of parking - {Settings.parkingCapacity}", Color.Yellow);
            print(" ", Color.None);
            print($"3---Change payment write-off period {Settings.intervalForPay} seconds", Color.Yellow);
            print(" ", Color.None);
            print($"4---Change period for writing to the Transaction.log, {Settings.intervalWriteIntoLog} seconds ", Color.Yellow);
            print(" ", Color.None);
            print($"5---Change coefficient of fine - {Settings.fine}", Color.Yellow);
            print(" ", Color.None);
            print("6---Change cost for a vehicle", Color.Yellow);
            print($"Passenger - {Settings.vehiclePrice[VehicleType.PassengerCar]}$   Truck - {Settings.vehiclePrice[VehicleType.Truck] }$   " +
                $"Bus - {Settings.vehiclePrice[VehicleType.Bus] }$   Motorcycle - {Settings.vehiclePrice[VehicleType.Motorcycle] }", Color.Yellow);
            print(" ", Color.None);
            print("ESC - back to menu", Color.Red);
        }
        public void DisplayParkingMenu()
        {
            Console.Clear();
            print("Click button 1-9", Color.Red);
            print(" ", Color.None);
            print("1---Display parking balance.", Color.Green);
            print(" ", Color.None);
            print("2---Display the amount of earned funds for the current period.", Color.Green);
            print(" ", Color.None);
            print("3---Display the number of free parking spaces.", Color.Green);
            print(" ", Color.None);
            print("4---Display all Parking Transactions for the current period.", Color.Green);          
            print(" ", Color.None);
            print("5---Display the transaction history of Transactions.log .", Color.Green);
            print(" ", Color.None);
            print("6---Display a list vehicles which are in the Parking lot.", Color.Green);
            print(" ", Color.None);
            print("7---Put the vehicle in the parking lot.", Color.Green);
            print(" ", Color.None);
            print("8---Remove the Vehicle from the Parking.", Color.Green);
            print(" ", Color.None);
            print("9---Top Up balance for a Vehicle.", Color.Green);
            print(" ", Color.None);
            print("ESC - back to menu", Color.Red);
        }
    }
}
