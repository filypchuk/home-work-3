﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Web.Http.Description;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet("balance")]
        [ResponseType(typeof(decimal))]
        public ActionResult<decimal> GetBalance()
        {            
            return _parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}
