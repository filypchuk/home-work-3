﻿using System.Linq;
using System.Text.RegularExpressions;
using CoolParking.BL.Dto;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet("last")]
        public ActionResult<TransactionInfo[]> GetLast()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        [HttpGet("all")]
        public ActionResult<TransactionInfo[]> GetAll()
        {
            string log = _parkingService.ReadFromLog();
            if (log == null || log == "")
            {
                return NotFound("Transaction not found");
            }
            return Ok(log);
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUpVehicleDto topUpVehicleDto)
        {
            Vehicle vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == topUpVehicleDto.Id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found");
            }
            else
            {
                _parkingService.TopUpVehicle(topUpVehicleDto.Id, topUpVehicleDto.Sum);
                return Ok(vehicle);
            }
        }
    }
}