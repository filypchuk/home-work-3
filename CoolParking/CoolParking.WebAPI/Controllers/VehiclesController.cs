﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using CoolParking.BL.Dto;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            return _parkingService.GetVehicles();
        }
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetById([FromRoute] string id)
        {
            string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
            if (!Regex.IsMatch(id, pattern))
            {
                ModelState.AddModelError("Id", "Format id is incorrect");
                return BadRequest(ModelState);
            }
            Vehicle vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (vehicle == null)
                return NotFound("Vehicle not found");
            return Ok(vehicle);
        }
        [HttpPost]
        public ActionResult<Vehicle> AddVehicle(VehicleDto vehicleDto)
        {
            _parkingService.AddVehicle(new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance));

            return CreatedAtAction(nameof(GetById), new { id = vehicleDto.Id }, vehicleDto);
        }
        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle([FromRoute] string id)
        {
            string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
            if (!Regex.IsMatch(id, pattern))
            {
                ModelState.AddModelError("Id", "Format id is incorrect");
                return BadRequest(ModelState);
            }
            Vehicle vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (vehicle == null)
            { 
                return NotFound("Vehicle not found"); 
            }
            else
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
        }
    }
}